//
//  ViewController.swift
//  ImagePickerDemo
//
//  Created by See.Ku on 2016/02/29.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit
import AssetsLibrary
import Photos

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var infoLabel: UILabel!

	@IBOutlet weak var photoLibraryButton: UIButton!
	@IBOutlet weak var cameraButton: UIButton!
	@IBOutlet weak var savedPhotosButton: UIButton!

	/// PhotoLibraryから画像を取得
	@IBAction func onPhotoLibrary(sender: AnyObject) {

		// ImagePickerを作成
		let ipc = UIImagePickerController()

		// delegateを設定
		ipc.delegate = self

		// PhotoLibraryから選択
		ipc.sourceType = .PhotoLibrary

		// PhotoLibraryはモーダルフォームで選択
		ipc.modalPresentationStyle = .FormSheet
		presentViewController(ipc, animated: true, completion: nil)
	}

	/// Cameraから画像を取得
	@IBAction func onCamera(sender: AnyObject) {

		// ImagePickerを作成
		let ipc = UIImagePickerController()
		ipc.delegate = self
		ipc.sourceType = .Camera

		// Cameraは全画面で選択
		presentViewController(ipc, animated: true, completion: nil)
	}

	/// SavedPhotosAlbumから画像を取得
	@IBAction func onSavedPhotos(sender: AnyObject) {

		// ImagePickerを作成
		let ipc = UIImagePickerController()
		ipc.delegate = self
		ipc.sourceType = .SavedPhotosAlbum

		// SavedPhotosAlbumはモーダルフォームで選択
		ipc.modalPresentationStyle = .FormSheet
		presentViewController(ipc, animated: true, completion: nil)
	}

	/// 画像を取得できた
	func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

		var str = ""

		// 圧縮後の情報を使うため、クラスを自力で生成
		let rep = SK4ShrinkRepresentation()
		let imageType = SK4ShrinkRepresentation.getImageType(info)

		// 編集前のイメージを取り出し
		if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {

			// 制限無しでデータ化 → 元々のデータサイズを調べる
			rep.maxDataByte = 0
			rep.maxLongSide = 0
			if let data = rep.makeData(image, imageType: imageType) {
				str += " Original: \(image.size.width)x\(image.size.height)"
				str += " \(data.length / 1024)KByte"
			} else {
				str += " Representation Error"
			}

			// 1MByte / 最大1024x1024に制限してデータ化
			rep.maxDataByte = 1024 * 1024
			rep.maxLongSide = 1024
			if let data = rep.makeData(image, imageType: imageType) {
				str += " Limited: \(rep.resizedSize.width)x\(rep.resizedSize.height)"
				str += " \(data.length / 1024)KByte"
				str += " Quality: \(rep.qualityUse)"

				imageView.image = UIImage(data: data)
			} else {
				str += " Representation Error"

				imageView.image = nil
			}
		}

		switch imageType {
		case .Png:
			str += " Type: PNG"
		case .Jpeg:
			str += " Type: JPG"
		case .Other:
			str += " Type: UNKOWN"
		}

		// 画像の情報を表示
		infoLabel.text = str

		// ImagePickerを終了
		dismissViewControllerAnimated(true, completion: nil)
	}

	/// キャンセルされた
	func imagePickerControllerDidCancel(picker: UIImagePickerController) {

		// ImagePickerを終了
		dismissViewControllerAnimated(true, completion: nil)
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		// 使用できるソースをチェック（今のところ.Camera以外はチェック不要？）

		// カメラは使えるか？
		if UIImagePickerController.isSourceTypeAvailable(.Camera) == false {
			cameraButton.enabled = false
		}

		// フォトライブラリは使えるか？
		if UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary) == false {
			photoLibraryButton.enabled = false
		}

		// アルバムは使えるか？（使えない事があるのか？）
		if UIImagePickerController.isSourceTypeAvailable(.SavedPhotosAlbum) == false {
			savedPhotosButton.enabled = false
		}
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

}

