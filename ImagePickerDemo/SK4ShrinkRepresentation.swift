//
//  SK4ShrinkRepresentation.swift
//  ImagePickerDemo
//
//  Created by See.Ku on 2016/03/06.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit

/// サイズや容量を制限した上でイメージをデータ化するクラス
public class SK4ShrinkRepresentation {

	/// 画像の種類
	public enum ImageType {
		case Png
		case Jpeg
		case Other
	}

	// /////////////////////////////////////////////////////////////
	// MARK: - プロパティ

	/// データ化した後の最大容量　※解像度にもよるけど512KByteぐらいが下限
	public var maxDataByte = 1024 * 1024 * 4

	/// 長辺の最大サイズ
	public var maxLongSide: CGFloat = 1024 * 2

	/// JPEG形式の圧縮率（最低／最高／差分）
	public var qualityMin: CGFloat = 0.05
	public var qualityMax: CGFloat = 0.95
	public var qualityDif: CGFloat = 0.15

	/// 実際に使用した圧縮率
	public var qualityUse: CGFloat = 0.0

	/// リサイズ後の画像サイズ
	public var resizedSize = CGSize()

	// /////////////////////////////////////////////////////////////
	// MARK: - メソッド

	/// 画像をデータ化
	public func makeData(image: UIImage, imageType: ImageType) -> NSData? {

		// 必要であればイメージをリサイズ
		let resize = resizeImage(image)

		// PNGが指定されたときは、PNG形式でデータ化を試す
		if imageType == .Png {
			let data = UIImagePNGRepresentation(resize)
			if checkDataSize(data) {
				qualityUse = 0
				return data
			}
		}

		// JPEG形式でデータ化
		for qualityUse = qualityMax; qualityUse >= qualityMin; qualityUse -= qualityDif {
			let data = UIImageJPEGRepresentation(resize, qualityUse)
			if checkDataSize(data) {
				return data
			}
		}

		// ギブアップ
		return nil
	}

	/// データ化した後の容量をチェック
	public func checkDataSize(data: NSData?) -> Bool {

		// 最大容量の指定が無い → 調べるまでも無く成功
		if maxDataByte == 0 {
			return true
		}

		// データ化に失敗している → そのまま終了
		guard let data = data else {
			return true
		}

		// 最大容量以下になった → 成功
		if data.length < maxDataByte {
			return true
		}

		// 最大容量を超えている → 失敗
		return false
	}

	/// イメージのサイズを変更
	func resizeImage(src: UIImage) -> UIImage {

		// リサイズが必要か？
		let ss = src.size
		if maxLongSide == 0 || ( ss.width <= maxLongSide && ss.height <= maxLongSide ) {
			resizedSize = ss
			return src
		}

		// TODO: リサイズ回りの処理を切りだし

		// リサイズ後のサイズを計算
		let ax = ss.width / maxLongSide
		let ay = ss.height / maxLongSide
		let ar = ax > ay ? ax : ay
		let re = CGRect(x: 0, y: 0, width: ss.width / ar, height: ss.height / ar)

		// リサイズ
		UIGraphicsBeginImageContext(re.size)
		src.drawInRect(re)
		let dst = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		resizedSize = dst.size

		return dst
	}

	// /////////////////////////////////////////////////////////////
	// MARK: - クラスメソッド

	/// 最大値を指定して画像をデータ化（imagePickerController:didFinishPickingMediaWithInfo:経由向け）
	public class func makeData(image: UIImage, maxDataByte: Int, maxLongSide: CGFloat, info: [String : AnyObject]) -> NSData? {
		let imageType = getImageType(info)
		return makeData(image, maxDataByte: maxDataByte, maxLongSide: maxLongSide, imageType: imageType)
	}

	/// 最大値を指定して画像をデータ化（それ以外の用途向け）
	public class func makeData(image: UIImage, maxDataByte: Int, maxLongSide: CGFloat, imageType: ImageType = .Png) -> NSData? {
		let rep = SK4ShrinkRepresentation()
		rep.maxDataByte = maxDataByte
		rep.maxLongSide = maxLongSide
		return rep.makeData(image, imageType: imageType)
	}

	/// imagePickerController:didFinishPickingMediaWithInfo:のinfoから画像の種類を取得
	public class func getImageType(info: [String : AnyObject]) -> ImageType {
		if let url = info[UIImagePickerControllerReferenceURL] as? NSURL {
			let abs = url.absoluteString.lowercaseString
			if abs.containsString("ext=png") {
				return .Png
			} else if abs.containsString("ext=jpg") {
				return .Jpeg
			}
		}
		return .Other
	}

}

// eof
