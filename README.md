
# ImagePickerDemo

これは、UIImagePickerController()の使い方について、簡単にまとめたデモアプリです。


### 動作を確認した環境

|環境	|情報			|
|-------|---------------|
|Xcode	|7.2.1 (7C1002)	|
|iOS	|9.2			|
|Swift	|2.1.1			|
|Date	|2016/3/6		|


### 内容

主に、以下のような項目についてまとめてあります。

* PhotoLibrary / Camera / SavedPhotosAlbumから画像を取得
* サイズや容量を制限した上でイメージをデータ化


### ライセンス

ソースコードのライセンスは CC0 とします。

Creative Commons — CC0 1.0 Universal  
http://creativecommons.org/publicdomain/zero/1.0/


### 履歴

#### 2016/03/05

* Deprecatedなメソッドを使っていたのを修正
* 画像の情報に対応中

#### 2016/03/06

* サイズ／容量を制限しての画像のデータ化に対応

